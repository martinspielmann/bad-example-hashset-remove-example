package de.martinspielmann.badexample.bugs.hashset.removeall;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.Alphanumeric.class)
class HashsetRemoveAllUnequalSizesTest extends HashsetRemoveAllTest {

    @Test
    void test1000() {
        long executionTimeMillis = getExecutionTimeMillisForSizes(1000, 999);
        System.out.println("Millis for 1000-999: " + executionTimeMillis);
    }

    @Test
    void test10000() {
        long executionTimeMillis = getExecutionTimeMillisForSizes(10000, 9999);
        System.out.println("Millis for 10000-9999: " + executionTimeMillis);
    }

    @Test
    void test100000() {
        long executionTimeMillis = getExecutionTimeMillisForSizes(100000, 99999);
        System.out.println("Millis for 100000-99999: " + executionTimeMillis);
    }

    @Test
    void test1000000() {
        long executionTimeMillis = getExecutionTimeMillisForSizes(1000000, 999999);
        System.out.println("Millis for 1000000-999999: " + executionTimeMillis);
    }

}
