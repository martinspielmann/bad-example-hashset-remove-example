package de.martinspielmann.badexample.bugs.hashset.removeall;

import static java.util.stream.Collectors.*;

import java.time.Duration;
import java.time.Instant;
import java.util.stream.IntStream;

abstract class HashsetRemoveAllTest {

    protected long getExecutionTimeMillisForSizes(int setSize, int listSize) {
        var set = IntStream.range(0, setSize).boxed().collect(toSet());
        var list = IntStream.range(0, listSize).boxed().collect(toList());

        var from = Instant.now();
        // remove the list from the set
        set.removeAll(list);
        var to = Instant.now();
        return Duration.between(from, to).toMillis();
    }

}
