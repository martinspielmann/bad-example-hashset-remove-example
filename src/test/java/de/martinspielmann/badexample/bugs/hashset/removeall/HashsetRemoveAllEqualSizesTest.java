package de.martinspielmann.badexample.bugs.hashset.removeall;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.Alphanumeric.class)
class HashsetRemoveAllEqualSizesTest extends HashsetRemoveAllTest {

    @Test
    void test1000() {
        long executionTimeMillis = getExecutionTimeMillisForSizes(1000, 1000);
        System.out.println("Millis for 1000-1000: " + executionTimeMillis);
    }

    @Test
    void test10000() {
        long executionTimeMillis = getExecutionTimeMillisForSizes(10000, 10000);
        System.out.println("Millis for 10000-10000: " + executionTimeMillis);
    }

    @Test
    void test100000() {
        long executionTimeMillis = getExecutionTimeMillisForSizes(100000, 100000);
        System.out.println("Millis for 100000-100000: " + executionTimeMillis);
    }

    @Test
    void test1000000() {
        long executionTimeMillis = getExecutionTimeMillisForSizes(1000000, 1000000);
        System.out.println("Millis for 1000000-1000000: " + executionTimeMillis);
    }

}
